# frozen_string_literal: true

require 'tempfile'
require 'open3'

module Jekyll
  class Stork
    attr_reader :site, :config

    STEMMERS = {
      'ar' => 'Arabic',
      'da' => 'Danish',
      'nl' => 'Dutch',
      'en' => 'English',
      'fi' => 'Finnish',
      'fr' => 'French',
      'de' => 'German',
      'el' => 'Greek',
      'hu' => 'Hungarian',
      'it' => 'Italian',
      'no' => 'Norwegian',
      'pt' => 'Portuguese',
      'ro' => 'Romanian',
      'ru' => 'Russian',
      'es' => 'Spanish',
      'sv' => 'Swedish',
      'ta' => 'Tamil',
      'tr' => 'Turkish'
    }.freeze

    # @param [Jekyll::Site]
    def initialize(site)
      @site = site
    end

    def index!
      Open3.popen2e('stork', '--build', config) do |_, stdout, thread|
        puts stdout.read

        if thread.value.success?
          site.static_files << Jekyll::StaticFile.new(site, site.source, '', file)
        end
      end
    rescue Errno::ENOENT
      Jekyll.logger.warn "I couldn't find the `stork` program on your PATH, maybe you need to install it?"
    end

    # Returns the locale from the Site.  Stork uses English as default
    # language so we're following that...
    #
    # @return [String]
    def locale
      @locale ||= site.config['locale'] || site.config['lang'] || 'en'
    end

    # @see locale
    # @return [String]
    def stemmer
      @stemmer ||= STEMMERS[locale] || 'English'
    end

    # @return [Hash]
    def files
      @files ||= site.documents.concat(site.pages).map do |page|
        {
          'path' => page.destination(site.dest),
          'url' => page.url,
          'title' => page.data['title'] || ''
        }
      end
    end

    # @return [String]
    def file
      @file ||= 'index.st'
    end

    # @return [String]
    def dest
      @dest ||= File.join(site.dest, file)
    end

    # @return [String]
    def config
      @config ||= Tempfile.open(%w[config toml]) do |c|
        c.write <<~EOC
          [input]
          files = #{files.to_s.gsub('"=>"', '" = "')}
          stemming = "#{stemmer}"

          [output]
          filename = "#{dest}"
        EOC

        c.path
      end
    end
  end
end
