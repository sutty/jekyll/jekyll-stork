# frozen_string_literal: true

require_relative 'jekyll/stork'

# We're hooking into post-write so we can index all HTML pages on the
# site.
Jekyll::Hooks.register :site, :post_write do |site|
  Jekyll::Stork.new(site).index!
end
