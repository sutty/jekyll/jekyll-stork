# jekyll-stork

An almost zero configuration plugin for [Stork
Search](https://stork-search.net/).

## Installation

Add this line to your site's Gemfile:

```ruby
gem 'jekyll-stork'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install jekyll-stork

## Usage

Add the plugin to your `_config.yml`:

```yaml
plugins:
- jekyll-stork
locale: es
lang: es
```

And that's it.  If you're writing in other languages supported by Stork,
set the `locale` or `lang` variable to a 2-letter ISO code in your
`_config.yml`.

The frontend part needs to be added separately into your theme by
following the quick start guide.

## Compression

If you want to compress the index, you can use
[jekyll-gzip](https://github.com/philnash/jekyll-gzip) and
[jekyll-brotli](https://github.com/philnash/jekyll-brotli).

Example configuration:

```yaml
---
plugins:
- jekyll-gzip
- jekyll-brotli
brotli:
  extensions:
  - '.st'
gzip:
  extensions:
  - '.st'
```

## TODO

* Support several languages at the same time.
* Index source files instead of pages?

## Contributing

Bug reports and pull requests are welcome on 0xacab.org at
<https://0xacab.org/sutty/jekyll/jekyll-stork>. This
project is intended to be a safe, welcoming space for collaboration, and
contributors are expected to adhere to the [Sutty code of
conduct](https://sutty.nl/en/code-of-conduct/).

If you like our plugins, [please consider
donating](https://donaciones.sutty.nl/en/)!

## License

The gem is available as free software under the terms of the GPL3
License.

## Code of Conduct

Everyone interacting in the jekyll-stork project’s
codebases, issue trackers, chat rooms and mailing lists is expected to
follow the [code of conduct](https://sutty.nl/en/code-of-conduct/).
